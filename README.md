# Bayesian parameter estimation of chaotic systems using the ABC-SMC algorithm with GP acceleration.
## Dependencies
 scikit-learn and GPy
 
 `pip install -U scikit-learn`
 
 `pip install GPy`

See the demo with a [Lorenz attractor](https://github.com/sanmitraghosh/ABC-Chaos/blob/master/lorenz.py) or [Rössler attractor, Jupyter notebook](https://github.com/sanmitraghosh/ABC-Chaos/blob/master/Rössler_attractor.ipynb)
